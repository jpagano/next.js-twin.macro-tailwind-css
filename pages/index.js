import Head from 'next/head'

import 'twin.macro'

import Main from '../components/Main'
import Button from '../components/Button'
import Card, { CardButton, CardHeader, CardSection, CardTitle } from '../components/Card'
import Grid from '../components/Grid'
import Heading from '../components/Heading'
import Label from '../components/Label'

export default function Home() {
  return (
    <div className="font-sans">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Main>
        <div tw="container mx-auto p-5">
          <div>
            <Label>Label</Label>
            <input type="text" placeholder=".medium-6.cell" />
          </div>
          <Button primary>Primary Button</Button>
          <Button secondary as="a" href="#">Secondary Button</Button>
          <div>
            <Button primary expanded>Expanded Button</Button>
          </div>
          <Heading level={2}>Heading</Heading>
          <Grid>
            {[1, 2, 3, 4].map(i => (
              <Card key={i}>
                <CardHeader>
                  <CardTitle>Card {i}</CardTitle>
                </CardHeader>
                <img src="https://source.unsplash.com/random/400x300" alt="Card" />
                <CardSection>
                  <CardTitle>Card {i}</CardTitle>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Vestibulum euismod, nisl sit amet consectetur consectetur,
                    nisi erat euismod nunc, eget consectetur nunc nisi sed
                    tellus.
                  </p>
                </CardSection>
                <CardButton primary>Card Button</CardButton>
              </Card>
            ))}
          </Grid>
        </div>
      </Main>
    </div>
  )
}
