import 'twin.macro'

import Header from '../components/Header'
import Main from '../components/Main'
import Container from '../components/Container'
import Footer from '../components/Footer'
import BackgroundShapes from '../components/BackgroundShapes'
import Hero from '../components/Hero'
import data from '../data/data.json'
// import React, { useEffect, useState } from 'react'

export default function Landing() {
  // const [data, setData] = useState(null)
  // const [loading, setLoading] = useState(true)
  //
  // let baseUrl = process.env.BASE_URL;
  // if ( process.env.VERCEL_URL ) {
  //   baseUrl = `https://${process.env.VERCEL_URL}`;
  // }

  // useEffect(() => {
  //   setLoading(true)
  //   fetch(`http://localhost:3000/api/landing`)
  //     .then(res => res.json())
  //     .then(data => {
  //       setData(data)
  //       setLoading(false)
  //     })
  // }, []);

  return (
    <>
      <Container>
        <Header />
        <Main tw="relative">
          {/*{ loading ? (*/}
          {/*  <div>*/}
          {/*    LOADING*/}
          {/*  </div>*/}
          {/*) : (*/}
            <>
              <Hero
                title={data.title}
                description={data.description}
                image={data.image}
                buttons={data.links}
              />
            </>
          {/*)}*/}
        </Main>
        <Footer />
        <BackgroundShapes />
      </Container>
    </>
  );
}

// export async function getServerSideProps() {
//   let baseUrl = process.env.BASE_URL;
//   if ( process.env.VERCEL_URL ) {
//     baseUrl = `https://${process.env.VERCEL_URL}`;
//   }
//
//   const response = await fetch(` ${baseUrl}/api/landing`)
//   const data = await response.json()
//
//   return {
//     props: {
//       data: data
//     },
//   };
// }
