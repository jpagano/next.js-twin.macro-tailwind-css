module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      small: '375px',
      medium: '768px',
      large: '1180px'
    },
    colors: {
      primary: '#97a783',
      secondary: '#aac29e',
      white: '#ffffff',
      black: '#000000',
      gray: {
        light: '#e6e6e6',
        medium: '#cacaca',
        dark: '#8a8a8a'
      },
    },
    fontSize: {
      small: ['14px', { lineHeight: '20px' }],
      normal: ['16px', { lineHeight: '24px' }],
      medium: ['18px', { lineHeight: '28px' }],
      large: ['20px', { lineHeight: '32px' }],
    },
    fontFamily: {
      sans: 'Inter, sans-serif',
      serif: 'Georgia, serif',
      mono: 'Menlo, monospace',
    },
    extend: {
      keyframes: {
        'moveLeftBounce': {
          '0%': {
            transform: 'translateX(0)'
          },
          '50%': {
            transform: 'translateX(5px)'
          },
          '100%': {
            transform: 'translateX(0)'
          }
        },
        'rotateme': {
          '0%': {
            transform: 'rotate(0deg)'
          },
          '100%': {
            transform: 'rotate(360deg)'
          }
        },
        'animationFramesOne': {
          '0%': {
            transform:  'translate(0px,0px) rotate(0deg)'
          },
          '20%': {
            transform:  'translate(73px,-1px)  rotate(36deg)'
          },
          '40%': {
            transform:  'translate(141px,72px)  rotate(72deg)'
          },
          '60%': {
            transform:  'translate(83px,122px)  rotate(108deg)'
          },
          '80%': {
            transform:  'translate(-40px,72px)  rotate(144deg)'
          },
          '100%': {
            transform:  'translate(0px,0px)  rotate(0deg)'
          }
        }
      }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms')
  ],
}
