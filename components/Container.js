import tw, { styled } from 'twin.macro'

const StyledContainer = styled.div(
  tw`
    mx-auto 
    relative
  `
)

const Container = ({ children, ...rest }) => (
  <StyledContainer {...rest}>
    {children}
  </StyledContainer>
)
export default Container
