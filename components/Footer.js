import tw, { styled } from 'twin.macro'

const StyledFooter = styled.footer(
  tw`
    bg-secondary
    text-white
  `
)

const FooterInner = styled.div(
  tw`
    container 
    mx-auto 
    px-4 
    py-8
  `
)

const Footer = () => (
  <StyledFooter>
    <FooterInner>
      <h2>FOOTER</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod, nisi vel consectetur egestas, nisl
        nunc aliquet nunc, eget euismod nisl nunc eget.</p>
    </FooterInner>
  </StyledFooter>
)
export default Footer
