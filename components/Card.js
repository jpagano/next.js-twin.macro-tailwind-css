import tw, {styled} from 'twin.macro'
import Button from './Button'
import Heading, { StyledHeading } from './Heading'

const Card = styled.div(
  tw`
    flex
    flex-col
    flex-grow
    bg-white
    text-black
  `
)

const CardHeader = styled.div(
  tw`
    flex
    flex-[1 0 auto]
    p-3
    bg-gray-light
  `
)

const CardSection = styled.div(
  tw`
    flex-grow
    flex-[1 0 auto]
    p-4
  `
)

const CardTitle = styled(Heading)(
  tw`
    
  `
)

const CardButton = styled(Button)(
  tw`
    rounded-none
  `
)

export default Card;
export { CardHeader, CardSection, CardTitle, CardButton };
