import tw, { styled } from 'twin.macro'
import Button from './Button'
import Menu from './Menu'

const HeaderInner = styled.div(
  tw`
    container 
    mx-auto 
    flex 
    items-center 
    justify-between 
    px-4 
    py-6
  `
)

const HeaderButton = styled(Button)(
  tw`
    bg-white
    border-white
    hover:(border-primary)
  `
)

const Header = () => (
  <header>
    <HeaderInner>
      <a>
        <img src="logo-white.png" alt="" width="150" height="102" />
      </a>
      <Menu />
      <HeaderButton as="a">Button</HeaderButton>
    </HeaderInner>
  </header>
)
export default Header
