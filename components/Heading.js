import tw, {styled} from 'twin.macro'

const StyledHeading = styled.h1(
  tw`
    text-large 
    font-bold 
    text-black
  `
)

const Heading = ({ level, ...rest }) => {
  return (
    <StyledHeading
      as={`h${level}`}
      {...rest}
    />
  )
};

Heading.defaultProps = {
  level: 1
};

export default Heading;
