import 'twin.macro'
import Button from './Button'
import tw, { styled } from 'twin.macro'

const HeroContainer = styled.div(
  tw`
    flex
    flex-col
    medium:(flex-row items-center justify-between)
  `
)

const HeroContent = styled.div(
  tw`
    relative
    mb-6
    medium:(mb-0 pr-8)
  `
)

const HeroTitle = styled.h1(
  tw`
    mb-6 
    font-bold 
    leading-tight
    text-[30px] 
    medium:text-[46px] 
    large:text-[62px] 
  `
)

const HeroText = styled.p(
  tw`
    mb-6
    text-large 
    text-[#7f7f7f] 
  `
)

const HeroButtonsContainer = styled.div(
  tw`
    space-y-4 
    large:space-y-0 
    large:space-x-4
  `
)

const HeroImage = styled.div(
  tw`
    relative
    self-center 
    medium:self-end
  `
)

const HeroContentShapes = () => (
  <div>
    <img src="./shape1.png" alt="" tw="absolute bottom-0 left-[-380px] max-w-[350px] animate-[moveLeftBounce 3s linear infinite]" />
    <img src="./shape30.png" alt="" tw="absolute top-[-30px] left-[-70px] max-w-[150px] opacity-50 z-[-1] animate-[moveLeftBounce 3s linear infinite]" />
    <img src="./shape28.png" alt="" tw="absolute top-[-50px] right-0 animate-[rotateme 20s linear infinite]" />
    <img src="./shape16.png" alt="" tw="absolute top-[-30px] left-[-110px] animate-[animationFramesOne 20s linear infinite]" />
    <img src="./shape16.png" alt="" tw="absolute bottom-[10px] left-[-50px] animate-[animationFramesOne 20s linear infinite]" />
    <img src="./shape14.png" alt="" tw="absolute bottom-0 right-0 bottom-[120px] animate-[moveLeftBounce 3s linear infinite]" />
  </div>
)

const HeroImageShapes = () => (
  <div>
    <img src="./shape31.png" alt="" tw="absolute bottom-[60px] left-[-60px] z-[-1] max-w-[350px] animate-[rotateme 20s linear infinite]" />
    <img src="./shape4.png" alt="" tw="absolute top-[-30px] left-[-70px] max-w-[150px] opacity-50 z-[-1] animate-[animationFramesOne 20s linear infinite]" />
    <img src="./shape2.png" alt="" tw="absolute bottom-[50px] right-0 animate-[rotateme 20s linear infinite]" />
    <img src="./shape5.png" alt="" tw="absolute top-0 right-0 animate-[moveLeftBounce 3s linear infinite]" />
  </div>
)

const Hero = ({ title, description, buttons }) => (
  <HeroContainer>
    <HeroContent>
      <HeroTitle dangerouslySetInnerHTML={{ __html: title }} />
      <HeroText dangerouslySetInnerHTML={{ __html: description }} />
      <HeroButtonsContainer>
        {buttons.map(({ title, url }, index) => (
          <Button
            primary={index === 0}
            key={title}
            as="a"
            href={url}>{title}</Button>
        ))}
      </HeroButtonsContainer>
      <HeroContentShapes />
    </HeroContent>

    <HeroImage>
      <img src="./hero-image.jpg" alt="Card" width="400" height="780" />
      <HeroImageShapes />
    </HeroImage>
  </HeroContainer>
)
export default Hero
