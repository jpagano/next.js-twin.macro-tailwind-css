import tw, { styled } from 'twin.macro'

const Grid = styled.div(
  tw`
    grid
    grid-cols-1
    gap-4
    small:grid-cols-2
    small:gap-8
    medium:grid-cols-3
    medium:gap-12
    large:grid-cols-4
    large:gap-16
  `
)

export default Grid
