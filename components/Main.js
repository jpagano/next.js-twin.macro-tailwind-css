import tw, { styled } from 'twin.macro'

const MainInner = styled.div(
  tw`
    container 
    mx-auto 
    px-4 
    py-10
  `
)

export default function Main({ children, ...rest }) {
  return (
    <main {...rest}>
      <MainInner>
        {children}
      </MainInner>
    </main>
  );
}
