import 'twin.macro'

const BackgroundShapes = () => (
  <div>
    <img src="./shape29.png" alt="" tw="absolute top-0 right-0 z-[-1] max-w-[400px]" />
  </div>
)
export default BackgroundShapes
