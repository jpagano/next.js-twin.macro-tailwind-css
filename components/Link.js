import tw, { styled } from 'twin.macro'

const StyledLink = ({ as, children, className, href }) => (
  <Link href={href} as={as} passHref>
    <a className={className}>{children}</a>
  </Link>
)

const Link = styled(StyledLink)(
  tw`
  `
)

export default Link
