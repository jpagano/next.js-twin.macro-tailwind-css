import tw, { styled } from 'twin.macro'

const MenuList = styled.ul(
  tw`
    flex 
    items-center 
    space-x-8
  `
)

export default function Menu() {
  return (
    <nav>
      <MenuList>
        <li><a>Menu item</a></li>
        <li><a>Menu item</a></li>
      </MenuList>
    </nav>
  )
}
