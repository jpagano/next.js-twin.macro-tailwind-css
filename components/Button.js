import tw, {styled} from 'twin.macro'
import PropTypes from 'prop-types'

const Button = styled.button(
  tw`
    appearance-none
    inline-block
    align-middle
    px-9 py-5
    border-[1px]
    border-[#a7b995]
    rounded-[30px]
    font-sans
    text-normal
    text-center
    text-primary
    font-bold
    leading-none
    cursor-pointer
    transition-colors
    ease-linear
    hover:(bg-primary text-white)
  `,
  ({primary}) => primary && tw`bg-primary text-white hover:(text-primary bg-white)`,
  ({secondary}) => secondary && tw`bg-secondary text-white`,
  ({expanded}) => expanded && tw`w-full`
);

Button.propTypes = {
  primary: PropTypes.bool,
  secondary: PropTypes.bool,
  expanded: PropTypes.bool
};

export default Button;
